export class Address {
  private _id: string;
  private _isCustomer: boolean;

  constructor(id: any, isCustomer: any) {
    this._id = id;
    if (isCustomer === 'string') {
      this._isCustomer = isCustomer === 'true';
    } else {
      this._isCustomer = isCustomer;
    }
  }

  public id(): string {
    return this._id;
  }

  public isCustomer(): boolean {
    return this._isCustomer;
  }

  public setId(value: string) {
    this._id = value;
  }

  public setIsCustomer(value: boolean) {
    this._isCustomer = value;
  }

  public generateRoomId(): string {
    return 'room' + (this._isCustomer ? '-c-' : '-t-') + this._id;
  }
}
