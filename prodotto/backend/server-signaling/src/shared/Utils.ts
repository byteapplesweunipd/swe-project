import { Address } from 'src/shared/Address';
import { User } from 'src/shared/User';


export const addUser = (user:User, users:Array<User>) : boolean => {
    const user_id:string = user.id();
    const exists=checkIfUserExist(user.id(),users);
    let isAdded=false;

    if(exists>-1){
      console.log("addUser: user already exists")
      users[exists]=user;
    }else{
      console.log("addUser: user added")
      users.push(user);
      isAdded=true;
    }
    return isAdded;
}

export const removeUser = (id:string, users:Array<User>) : boolean => {
  const exists = checkIfUserExist(id,users);
  let isRemoved = false;


  if(exists>-1){
    console.log("removeUser: user removed")
    users.splice(exists);
    isRemoved=true;

  }else{
    console.log("removeUser: user does not exists")
  }

  return isRemoved;
}

export const checkIfUserExist = (id:string, users:Array<User>) : number => {
  let index = -1;
  for(let i=0; i<users.length; i++){
    if(users[i].id()==id){
      index = i;
    }
  }
  return index;
}


export const isEmpty = (users:Array<User>) : boolean => {
  return users.length===0?true:false;
}