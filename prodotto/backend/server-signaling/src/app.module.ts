import { Module } from '@nestjs/common';
import { AppGateway } from './gateway/app.gateway';

@Module({
  imports: [],
  controllers: [],
  providers: [AppGateway],
})
export class AppModule {}