import {Address} from "src/shared/Address";

describe("Test suite for Address class", () => {
  
  const obj = {id:"1", isCustomer:'false'}
  const obj2 = {id:"1", isCustomer:true}
  const addr_one_par:Address = new Address(obj);
  const addr_two_par:Address = new Address(obj2.id,obj2.isCustomer);
  
  test("defines id() get function", () => {
    expect(typeof addr_one_par.id).toBe("function");
  });
  test("defines isCustomer() function", () => {
    expect(typeof addr_one_par.isCustomer).toBe("function");
  });
  test("defines setId function", () => {
    expect(typeof addr_one_par.setId).toBe("function");
  });
  test("defines setIsCustomer function", () => {
    expect(typeof addr_one_par.setIsCustomer).toBe("function");
  });
  test("defines generateRoomId()  function", () => {
    expect(typeof addr_one_par.generateRoomId).toBe("function");
  });

  it("should construct an Address object based on the passed param (only string in the passed object)", () => {
    const tmp = new Address(obj);

    expect(tmp.id()).toBe(obj.id);
    expect(tmp.isCustomer()).toBe(obj.isCustomer==='true');
    expect(tmp).toBeInstanceOf(Address);
  });
  it("should construct an Address object based on the passed param (not only string in the passed object, also booleans)", () => {
    const tmp = new Address(obj2);

    expect(tmp.id()).toBe(obj2.id);
    expect(tmp.isCustomer()).toBe(obj2.isCustomer);
    expect(tmp).toBeInstanceOf(Address);
  });
  it("should construct an Address object based on the two passed params (only string in the passed params)", () => {
    const tmp:Address = new Address(obj.id,obj.isCustomer);

    expect(tmp.id()).toBe(obj.id);
    expect(tmp.isCustomer()).toBe(obj.isCustomer==='true');
    expect(tmp).toBeInstanceOf(Address);
  });
  it("should construct an Address object based on the two passed params (not only string in the passed params, also booleans)", () => {
    const tmp:Address = new Address(obj2.id,obj2.isCustomer);

    expect(tmp.id()).toBe(obj2.id);
    expect(tmp.isCustomer()).toBe(obj2.isCustomer);
    expect(tmp).toBeInstanceOf(Address);
  });
  it("should return the id attribute", () => {
    expect(addr_one_par.id()).toBe("1");
  });

  it("should return the isCustomer attribute", () => {
    expect(addr_one_par.isCustomer()).toBeFalsy();
  })

  it('should return the generated room id (user is not a customer)', () => {
    expect(addr_one_par.generateRoomId()).toBe('room-t-1');
  });
  it('should return the generated room id (user is a customer)', () => {
    expect(addr_two_par.generateRoomId()).toBe('room-c-1');
  });

  it("should set the id attribute", () => {
    const tmp = new Address(obj);
    tmp.setId('2')
    expect(tmp.id()).toBe("2");
  })

  it("should set the isCustomer attribute", () => {
    const tmp = new Address(obj);
    tmp.setIsCustomer(true);
    expect(tmp.isCustomer()).toBeTruthy;
  })

});
