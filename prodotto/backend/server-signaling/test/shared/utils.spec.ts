import { User } from 'src/shared/User';
import {
  addUser,
  removeUser,
  checkIfUserExist,
  isEmpty,
} from '../../src/shared/Utils';

describe('Test suite for addUser function', () => {
  const usr1 = new User({
    id: '1',
    name: 'name_test',
    surname: 'surname_test',
    email: 'email_test',
    number: 'number_test',
    isCustomer: 'true',
    isAvailable: 'true',
  });
  const usr2 = new User({
    id: '2',
    name: 'name_test',
    surname: 'surname_test',
    email: 'email_test',
    number: 'number_test',
    isCustomer: 'true',
    isAvailable: 'true',
  });
  const usr3 = new User({
    id: '3',
    name: 'name_test',
    surname: 'surname_test',
    email: 'email_test',
    number: 'number_test',
    isCustomer: 'true',
    isAvailable: 'true',
  });

  const users: Array<User> = [usr1, usr2];

  it('should return true if the user is added', async () => {
    expect(addUser(usr3, users)).toBe(true);
  });

  it('should return false if the user is already on the users array', async () => {
    expect(addUser(usr1, users)).toBe(false);
  });

  it('should add an user to the users array', async () => {
    const tmp: Array<User> = [usr1, usr2];
    const arr_len = tmp.length;
    addUser(usr3, users);
    expect(tmp.length).toBe(arr_len + 1);
  });
});

describe('Test suite for removeUser function', () => {
  const usr1 = new User({
    id: '1',
    name: 'name_test',
    surname: 'surname_test',
    email: 'email_test',
    number: 'number_test',
    isCustomer: 'true',
    isAvailable: 'true',
  });
  const usr2 = new User({
    id: '2',
    name: 'name_test',
    surname: 'surname_test',
    email: 'email_test',
    number: 'number_test',
    isCustomer: 'true',
    isAvailable: 'true',
  });
  const usr3 = new User({
    id: '3',
    name: 'name_test',
    surname: 'surname_test',
    email: 'email_test',
    number: 'number_test',
    isCustomer: 'true',
    isAvailable: 'true',
  });

  const users: Array<User> = [usr1, usr2];

  it('should return true if the user is removed', async () => {
    expect(removeUser('1', users)).toBe(true);
  });

  it('should return false if the user is not on the users array', async () => {
    expect(removeUser('3', users)).toBe(false);
  });

  it('should remove an user to the users array', async () => {
    const tmp: Array<User> = [usr1, usr2];
    const arr_len = tmp.length;
    addUser(usr3, users);
    expect(tmp.length).toBe(arr_len - 1);
  });
});

describe('Test suite for checkIfUserExist function', () => {
  const usr1 = new User({
    id: '1',
    name: 'name_test',
    surname: 'surname_test',
    email: 'email_test',
    number: 'number_test',
    isCustomer: 'true',
    isAvailable: 'true',
  });
  const usr2 = new User({
    id: '2',
    name: 'name_test',
    surname: 'surname_test',
    email: 'email_test',
    number: 'number_test',
    isCustomer: 'true',
    isAvailable: 'true',
  });
  const usr3 = new User({
    id: '3',
    name: 'name_test',
    surname: 'surname_test',
    email: 'email_test',
    number: 'number_test',
    isCustomer: 'true',
    isAvailable: 'true',
  });

  const users: Array<User> = [usr1, usr2];
  const users_empty: Array<User> = [];

  it('should return the index on the array of the checked user', async () => {
    expect(checkIfUserExist('1', users)).toEqual(0);
  });

  it('should return -1', async () => {
    expect(checkIfUserExist('3', users)).toEqual(-1);
  });

  it('should return -1', async () => {
    expect(checkIfUserExist('3', users_empty)).toEqual(-1);
  });
});

describe('Test suite for isEmpty function', () => {
  const usr1 = new User({
    id: '1',
    name: 'name_test',
    surname: 'surname_test',
    email: 'email_test',
    number: 'number_test',
    isCustomer: 'true',
    isAvailable: 'true',
  });

  const users: Array<User> = [usr1];
  const users_empty: Array<User> = [usr1];

  it('should return false if the passed array is not empty', async () => {
    expect(isEmpty(users)).toBe(false);
  });

  it('should return false if the passed array is empty', async () => {
    expect(isEmpty(users_empty)).toBe(true);
  });
});
