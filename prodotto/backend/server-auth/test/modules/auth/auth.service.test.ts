import { SequelizeModule, InjectModel, getModelToken } from "@nestjs/sequelize";
import { Test, TestingModule } from "@nestjs/testing";
import { AuthService } from "src/modules/auth/service/auth.service";
import { Users } from "src/shared/entities/user.entity";

describe("AuthService test suite", () => {
  let service: AuthService;

  const fakeUsers = [
    {
      id: 1,
      email: "prova",
      name: "name_prova",
      surname: "surname_prova",
      phoneNumber: "number_prova",
      password: "prova",
      createdAt: "2022-04-19T17:53:39.260Z",
      updatedAt: "2022-04-19T17:53:39.260Z",
    },
  ];

  const mockLoginBodySuccess = { email: "prova", password: "prova" };
  const mockLoginBodyFail1 = {
    email: "prova",
    password: "wrong",
  };
  const mockLoginBodyFail2 = {
    email: "doesnotexist",
    password: "doesnotexist",
  };

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        AuthService,
        {
          provide: getModelToken(Users),
          useValue: {
            findOne: jest.fn(() => fakeUsers[0]),
          },
        },
      ],
    }).compile();
    service = module.get<AuthService>(AuthService);
  });

  it("should be defined", () => {
    expect(service).toBeDefined();
  });

  test("define as User", () => {
    expect(service).toBeInstanceOf(AuthService);
  });

  it("should create a login POST - login success", async () => {
    const ret = await service.login(mockLoginBodySuccess);
    expect(ret).toEqual({
      success: true,
      data: fakeUsers[0],
    });
  });

  it("should create a login POST - login failed case 1: wrong psw", async () => {
    const ret = await service.login(mockLoginBodyFail1);
    expect(ret).toEqual({
      success: false,
      data: "Auth failed.",
    });
  });

  it("should create a login POST - login failed case 2: user does not exist", async () => {
    const ret = await service.login(mockLoginBodyFail2);
    expect(ret).toEqual({
      success: false,
      data: "Auth failed.",
    });
  });
});
