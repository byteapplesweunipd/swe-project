export default class AuthError {
  private _type: number;
  private _msg: string;

  constructor(type = 0, msg = "generic error") {
    this._type = type;
    this._msg = msg;
  }

  public type(): number {
    return this._type;
  }

  public msg(): string {
    return this._msg;
  }
}
