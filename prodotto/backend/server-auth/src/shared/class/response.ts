export default class Response {
  private success: boolean;
  private data: object;
  private error: string;
  private status: number;

  constructor(
    success: boolean,
    data?: object,
    error?: string,
    status?: number
  ) {
    this.success = success;
    this.data = typeof data === "undefined" ? null : data;
    this.error = typeof error === "undefined" ? null : error;
    this.status = typeof status === "undefined" ? null : status;
  }

  public getSuccess(): boolean {
    return this.success;
  }

  public getData(): object {
    return this.data;
  }

  public getError(): string {
    return this.error;
  }

  public getStatus(): number {
    return this.status;
  }

  public setStatus(status: number): void {
    this.status = status;
  }
}
