import { Module } from "@nestjs/common";
import { SequelizeModule } from "@nestjs/sequelize";
import { Users } from "../../shared/entities/user.entity";

@Module({
  imports: [
    SequelizeModule.forRoot({
      dialect: "postgres",
      host: "",
      port: 5432,
      username: "admin",
      password: "example",
      database: "postgres",
      models: [Users],
      autoLoadModels: true,
      synchronize: true,
    }),
  ],
})
export class DatabaseModule {}
