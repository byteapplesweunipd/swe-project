import { Body, Controller, Post, HttpStatus } from "@nestjs/common";
import { AuthService } from "../service/auth.service";
import Response from "src/shared/class/response";
import LoginInfo from "src/shared/class/loginInfo";

@Controller("auth")
export class AuthController {
  constructor(private readonly authService: AuthService) {}

  @Post("login")
  async login(@Body() body: LoginInfo): Promise<Response> {
    const res = await this.authService.login(body);
    if (res.getSuccess()) {
      res.setStatus(HttpStatus.OK);
    } else {
      res.setStatus(HttpStatus.UNAUTHORIZED);
    }
    return res;
  }
}
