import { Injectable } from "@nestjs/common";
import { InjectModel } from "@nestjs/sequelize";
import { Users } from "src/shared/entities/user.entity";
import AuthError from "src/shared/class/error";
import LoginInfo from "src/shared/class/LoginInfo";
import Response from "src/shared/class/Response";

@Injectable()
export class AuthService {
  constructor(@InjectModel(Users) private userModel: typeof Users) {}

  async login(body: LoginInfo): Promise<Response> {
    console.log(body.email);

    const user = await this.userModel.findOne({
      where: { email: body.email },
    });

    if (user == null) {
      const error: AuthError = new AuthError(1, "Auth failed.");
      const ret = new Response(false, {}, error.msg());
      return ret;
    }
    if (user.password === body.password) {
      user.password = "";
      const ret = new Response(true, user);
      return ret;
    } else {
      const error: AuthError = new AuthError(2, "Auth failed.");
      const ret = new Response(false, {}, error.msg());
      return ret;
    }
  }
}
