import 'package:flutter/material.dart';
import 'package:smart4energy/services/socket_service.dart';
import 'package:smart4energy/models/user.dart';
import 'package:get/get.dart';

class LoginController extends GetxController {
  List users = [];
  User user = User();
  GlobalKey<FormState> connectFormKey = GlobalKey<FormState>();

  connectToSocket() {
    if (connectFormKey.currentState!.validate()) {
      Get.find<SocketService>().handleSocket();
    }
  }

  addUsers(data) {
    users.clear();
    for (var item in data) {
      User newUser = User.fromJson(item);
      if (newUser.username != user.username) {
        users.add(User.fromJson(item));
      }
      // users.add(User.fromJson(item));
    }
    update();
  }
}
