import 'package:smart4energy/controllers/video_conference_controller.dart';
import 'package:get/get.dart';

class VideoBinding extends Bindings {
  @override
  void dependencies() {
    Get.put(VideoConferenceController());
  }
}
