import 'package:flutter/material.dart';
import 'package:smart4energy/pages/homePage.dart';

enum ConfirmationDialog {
  yes,
  no,
}

class AlarmsPage extends StatefulWidget {
  @override
  _AlarmsPageState createState() => _AlarmsPageState();
}

class _AlarmsPageState extends State<AlarmsPage> {
  @override
  Widget build(BuildContext context) => Scaffold(
      appBar: AppBar(
        title: const Text("Alarms"),
        centerTitle: true,
      ),
      body: ListView.builder(
          shrinkWrap: true,
          padding: const EdgeInsets.all(8.0),
          itemCount: interpreter.getCurrentActiveAlarms().length,
          itemBuilder: (context, index) {
            return ListTile(
              title: Text(interpreter.getCurrentActiveAlarms()[index]),
            );
          }));
}
