import 'package:flutter/material.dart';
import 'package:smart4energy/pages/homePage.dart';

enum ConfirmationDialog {
  yes,
  no,
}

class StatusPage extends StatefulWidget {
  @override
  _StatusPageState createState() => _StatusPageState();
}

class _StatusPageState extends State<StatusPage> {
  late List<String> statusData;
  int statusCounter = 0;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    statusData = interpreter.getCurrentActiveStates();
    statusCounter = interpreter.getCurrentActiveStates().length;
  }

  @override
  Widget build(BuildContext context) => Scaffold(
        appBar: AppBar(
          title: const Text("Status"),
          centerTitle: true,
        ),
        body: ListView.builder(
            shrinkWrap: true,
            padding: const EdgeInsets.all(8.0),
            itemCount: statusCounter,
            itemBuilder: (context, index) {
              return ListTile(
                title: Text(statusData[index]),
              );
            }),
      );
}
