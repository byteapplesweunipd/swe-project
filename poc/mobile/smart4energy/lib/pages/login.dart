import 'package:flutter/material.dart';
import 'package:smart4energy/controllers/login_controller.dart';
import 'package:get/get.dart';

class LoginScreen extends GetView<LoginController> {
  const LoginScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("Login"),
        centerTitle: true,
      ),
      body: Padding(
        padding: const EdgeInsets.symmetric(vertical: 10, horizontal: 20),
        child: Form(
          key: controller.connectFormKey,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              TextFormField(
                onChanged: (value) {
                  controller.user.username = value;
                },
                validator: (String? value) {
                  if (value!.isEmpty) {
                    return "Can not blank";
                  }
                  return null;
                },
                decoration: const InputDecoration(
                  labelText: "Username",
                  prefixIcon: Icon(
                    Icons.person,
                  ),
                  focusedBorder: OutlineInputBorder(
                      borderSide: BorderSide(color: Colors.orange)),
                  enabledBorder: OutlineInputBorder(
                      borderSide: BorderSide(color: Colors.orange)),
                  border: OutlineInputBorder(
                      borderSide: BorderSide(color: Colors.orange)),
                ),
              ),
              const SizedBox(height: 15),
              TextFormField(
                onChanged: (value) {
                  controller.user.name = value;
                },
                validator: (String? value) {
                  if (value!.isEmpty) {
                    return "Can not blank";
                  }
                  return null;
                },
                decoration: const InputDecoration(
                    labelText: "Name",
                    prefixIcon: Icon(
                      Icons.person,
                    ),
                    focusedBorder: OutlineInputBorder(
                        borderSide: BorderSide(color: Colors.orange)),
                    enabledBorder: OutlineInputBorder(
                        borderSide: BorderSide(color: Colors.orange)),
                    border: OutlineInputBorder(
                        borderSide: BorderSide(color: Colors.orange)),
                ),
              ),
              const SizedBox(height: 10),
              TextFormField(
                onChanged: (value) {
                  controller.user.surname = value;
                },
                validator: (String? value) {
                  if (value!.isEmpty) {
                    return "Can not blank";
                  }
                  return null;
                },
                decoration: const InputDecoration(
                  labelText: "Surname",
                  prefixIcon: Icon(
                    Icons.person,
                  ),
                  focusedBorder: OutlineInputBorder(
                      borderSide: BorderSide(color: Colors.orange)),
                  enabledBorder: OutlineInputBorder(
                      borderSide: BorderSide(color: Colors.orange)),
                  border: OutlineInputBorder(
                      borderSide: BorderSide(color: Colors.orange)),
                ),
              ),
              const SizedBox(height: 15),
              Container(
                width: Get.width,
                height: 45,
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(16),
                ),
                child: MaterialButton(
                    child: const Text(
                      'Connect',
                      style: TextStyle(color: Colors.black, fontSize: 18),
                    ),
                    color: Colors.orange,
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(16)),
                    onPressed: controller.connectToSocket,
              ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
