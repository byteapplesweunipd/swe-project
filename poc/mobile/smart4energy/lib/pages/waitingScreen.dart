import 'package:flutter/material.dart';
import 'package:smart4energy/controllers/login_controller.dart';
import 'package:smart4energy/models/call.dart';
import 'package:smart4energy/models/user.dart';
import 'package:smart4energy/routes/app_routes.dart';
import 'package:get/get.dart';

class WaitingScreen extends GetView<LoginController> {
  const WaitingScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Color(0xFFf2f2f2),
      appBar: AppBar(
        leading: Center(
          child: Text(
            'Video Call',
            style: TextStyle(fontSize: 25),
            textAlign: TextAlign.start,
          ),
        ),
        leadingWidth: double.infinity,
        backgroundColor: Colors.orange,
      ),
      body: GetBuilder<LoginController>(
        builder: (_) => controller.users.isEmpty
            ? Center(
                child: Text(
                  "No users found",
                  style: TextStyle(
                    fontSize: 20,
                    color: Colors.grey.shade500,
                  ),
                ),
              )
            : ListView.builder(
                itemCount: controller.users.length,
                itemBuilder: (context, index) {
                  User user = controller.users[index];
                  return Container(
                    width: Get.width,
                    margin: EdgeInsets.all(10),
                    child: Row(
                      children: [
                        CircleAvatar(
                          radius: 27.5,
                          child: Text(
                            "${user.name![0]}${user.surname![0]}",
                            style: TextStyle(color: Colors.white, fontSize: 25),
                          ),
                          backgroundColor: Colors.orange,
                        ),
                        SizedBox(width: 10),
                        Text(
                          "${user.name!} ${user.surname!}",
                          style: TextStyle(fontSize: 20),
                        ),
                        Spacer(),
                        IconButton(
                            onPressed: () {
                              Get.toNamed(
                                AppRoutes.VIDEOCALLSCREEN,
                                arguments: {
                                  "is_offer": true,
                                  "call": Call(
                                    to: controller.users[index],
                                    from: controller.user,
                                    isVideoCall: true,
                                  ),
                                },
                              );
                            },
                            icon: const Icon(Icons.video_call)),
                      ],
                    ),
                  );
                },
              ),
      ),
    );
  }
}
