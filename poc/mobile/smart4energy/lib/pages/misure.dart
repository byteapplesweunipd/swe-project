import 'package:flutter/material.dart';
import 'package:smart4energy/pages/homePage.dart';

enum ConfirmationDialog {
  yes,
  no,
}

class MeasurementsPage extends StatefulWidget {

  @override
  _MeasurementsPageState createState() => _MeasurementsPageState();
}

class _MeasurementsPageState extends State<MeasurementsPage> {
  @override
  Widget build(BuildContext context) => Scaffold(
        appBar: AppBar(
          title: const Text("Measurements"),
          centerTitle: true,
        ),
        body: ListView.builder(
            shrinkWrap: true,
            padding: const EdgeInsets.all(8.0),
            itemCount: interpreter.getCurrentMeasuresAllInOne().length,
            itemBuilder: (context, index) {
              return ListTile(
                title: Text(interpreter.getCurrentMeasuresAllInOne()[index]),
              );
            }),
      );
}
