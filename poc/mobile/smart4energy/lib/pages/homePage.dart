import 'package:flutter/material.dart';
import '../emulatore/modbus_master.dart';
import '../emulatore/modbus_data_interpreter.dart';
import 'package:smart4energy/pages/ActionSelector.dart';

late ModbusDataInterpreter interpreter;
late ModbusMaster master;
Future<ModbusDataInterpreter> creatingInterpreter =
    ModbusDataInterpreter.create();

class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  var _textController = TextEditingController();
  late bool _isButtonDisabled;
  bool intepreterCreated = false;

  @override
  void initState() {
    super.initState();
    _isButtonDisabled = false;
    intepreterCreated = false;
  }

  @override
  Widget build(BuildContext context) {
    creatingInterpreter.then((value) {
      interpreter = value;
      if (!intepreterCreated) {
        setState(() {
          intepreterCreated = true;
        });
      }
    });
    return Scaffold(
        appBar: AppBar(
          title: const Text("Welcome"),
        ),
        body: ListView(
          children: <Widget>[
            const ListTile(
              title: Text("Connect to the ModbusEmulator to start"),
            ),
            ListTile(
              title: TextFormField(
                controller: _textController,
                decoration: const InputDecoration(
                  focusedBorder: OutlineInputBorder(
                      borderSide: BorderSide(color: Colors.orange)),
                  enabledBorder: OutlineInputBorder(
                      borderSide: BorderSide(color: Colors.orange)),
                  border: OutlineInputBorder(
                      borderSide: BorderSide(color: Colors.orange)),
                  hintText: "Type IP adress...",
                ),
              ),
            ),
            ListTile(
                title: ElevatedButton(
                    child: Text(_isButtonDisabled ? "Wait..." : "Connect"),
                    onPressed: () {
                      setState(() => _isButtonDisabled = true);
                      String ipAdress = _textController.text;
                      master = ModbusMaster(
                          ipAddress: ipAdress, port: 8888, slaveId: 1);
                      _connection();
                    }))
          ],
        ));
  }

  Future<void> _connection() async {
    await master.connect();
    interpreter.master = master;
    Navigator.push(
        context, MaterialPageRoute(builder: (context) => NavigationPage()));
    _textController.clear();
    setState(() {
      _isButtonDisabled = false;
    });
  }
}
