import 'package:get/get.dart';
import 'package:smart4energy/pages/allarmi.dart';
import 'package:smart4energy/pages/misure.dart';
import 'package:smart4energy/pages/videocall.dart';
import 'package:smart4energy/pages/stati.dart';
import 'package:smart4energy/pages/login.dart';
import 'package:smart4energy/routes/app_routes.dart';
import 'package:flutter/material.dart';

class NavigationPage extends StatefulWidget {
  @override
  _NavigationPageState createState() => _NavigationPageState();
}

class _NavigationPageState extends State<NavigationPage> {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(
          title: const Text('Smart4Energy'),
          backgroundColor: Colors.orange,
        ),
        body: ListView(
          shrinkWrap: true,
          padding: const EdgeInsets.all(0.0),
          children: <Widget>[
            ListBody(
              children: <Widget>[
                ListTile(
                    title: const Text("Ask for remote Support"),
                    onTap: () => Get.to(() => const LoginScreen()),
                    trailing: const Icon(Icons.keyboard_arrow_right)),
                ListTile(
                    title: const Text("Measurements"),
                    onTap: () => Get.to(() => MeasurementsPage()),
                    trailing: const Icon(Icons.keyboard_arrow_right)),
                ListTile(
                    title: const Text("Alarms"),
                    onTap: () => Get.to(() => AlarmsPage()),
                    trailing: const Icon(Icons.keyboard_arrow_right)),
                ListTile(
                    title: const Text("Status"),
                    onTap: () => Get.to(() => StatusPage()),
                    trailing: const Icon(Icons.keyboard_arrow_right)),
              ],
            ),
          ],
        ),
      ),
    );
  }
}
