class AppRoutes {
  AppRoutes._();
  static const HOMEPAGE = '/HomePage';
  static const ALARMS = '/Alarms';
  static const MEASUREMENTS = '/Measurements';
  static const STATUS = '/Status';
  static const LOGIN = '/Login';
  static const VIDEOCALLSCREEN = '/Video';
  static const WAITING = '/Waiting';
}
