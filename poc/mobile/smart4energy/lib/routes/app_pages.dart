import 'package:smart4energy/pages/allarmi.dart';
import 'package:smart4energy/pages/homePage.dart';
import 'package:smart4energy/pages/login.dart';
import 'package:smart4energy/pages/misure.dart';
import 'package:smart4energy/pages/videocall.dart';
import 'package:smart4energy/pages/stati.dart';
import 'package:smart4energy/bindings/video_binding.dart';
import 'package:get/get.dart';
import 'package:smart4energy/pages/waitingScreen.dart';
import 'app_routes.dart';

class AppPages {
  static const INITIAL_ROUTE = AppRoutes.HOMEPAGE;
  static final List<GetPage> pages = [
    GetPage(
      name: AppRoutes.ALARMS,
      page: () => AlarmsPage(),
    ),
    GetPage(
      name: AppRoutes.MEASUREMENTS,
      page: () => MeasurementsPage(),
    ),
    GetPage(
      name: AppRoutes.STATUS,
      page: () => StatusPage(),
    ),
    GetPage(
      name: AppRoutes.HOMEPAGE,
      page: () => HomePage(),
    ),
    GetPage(
      name: AppRoutes.LOGIN,
      page: () => LoginScreen(),
    ),
    GetPage(
      name: AppRoutes.WAITING,
      page: () => WaitingScreen(),
    ),
    GetPage(
      name: AppRoutes.VIDEOCALLSCREEN,
      page: () => VideoConferenceScreen(),
      binding: VideoBinding(),
    ),
  ];
}
