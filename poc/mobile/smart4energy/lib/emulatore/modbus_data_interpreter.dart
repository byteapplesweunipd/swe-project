import 'modbus_master.dart';
import 'dart:typed_data';
import 'dart:async';
import 'package:excel/excel.dart';
import 'excel_reader.dart';
import 'converter.dart';

class ModbusDataInterpreter {
  List<String> _currentActiveStates = [];
  List<String> _currentActiveAlarms = [];
  Int16List _currentMeasureValues = Int16List.fromList([]);
  List<String> _currentMeasuresAllInOne = [];
  late final List<String> _statesNames;
  late final List<String> _alarmsNames;
  late final List<String> _measureNames;
  late final List<String> _unitOfMeasure;
  late final List<String> _firstFormatOfMeasures;
  late final List<String> _secondFormatOfMeasures;
  late String _measurementsControlManagementTable;
  late int _formatType;
  ModbusMaster? _master;
  bool _resetRecently = true;
  bool _dataChanged = false;

  ModbusDataInterpreter._();

  static Future<ModbusDataInterpreter> create([ModbusMaster? master]) async {
    ModbusDataInterpreter interpreter = ModbusDataInterpreter._();
    await interpreter._init();
    interpreter.master = master;
    return interpreter;
  }

  Future<void> _init() async {
    //Leggo i nomi degli stati e degli allarmi da file lingue
    Excel fileLingue = await getExcel("assets/texts.xlsm");
    Sheet? translationSheet = getSheet(fileLingue, "Translation");
    _statesNames = getColumnRange(translationSheet, 285, 412, "H");
    _alarmsNames = getColumnRange(translationSheet, 156, 283, "H");

    //Leggo i nomi delle misure, le unità di misura e i due tipi di formati
    Excel misure = await getExcel("assets/misure.xlsx");
    Sheet? measuresSheet = getSheet(misure, "Misure");
    _measureNames = getColumnRange(measuresSheet, 1, 80, "A");
    _unitOfMeasure = getColumnRange(measuresSheet, 1, 80, "B");
    _firstFormatOfMeasures = getColumnRange(measuresSheet, 1, 80, "C");
    _secondFormatOfMeasures = getColumnRange(measuresSheet, 1, 80, "D");
  }

  Future<void> _begin() async {
    _resetRecently = false;
    await _readMCMT(_master);
    await _readFormatType(_master);
    _updateData(_master);
  }

  Future<void> _updateData(ModbusMaster? master) async {
    try {
      while (!master!.isClosed) {
        await _updateCurrentActiveStates(
            await master.readHoldingRegisters(0x0030, 8));
        await _updateCurrentActiveAlarms(
            await master.readHoldingRegisters(0x0038, 8));
        await _updateCurrentActiveMeasures(
            await master.readHoldingRegisters(0x0040, 80));
        await Future.delayed(const Duration(seconds: 1));
      }
    } catch (error) {
      if (!_resetRecently) {
        // ignore: avoid_print
        print("Master Socket has been closed!");
        _reset();
      }
      if (_dataChanged) {
        _reset();
      }
    }
  }

  Future<void> _updateCurrentActiveStates(Uint8List statesRx) async {
    _dataChanged = true;
    String statesRxBinary = "";
    for (var i = 3; i < statesRx.length - 2; i++) {
      statesRxBinary += uIntTo8bitString(statesRx[i]);
    }
    List<String> temp = [];
    for (int i = 0; i < statesRxBinary.length; i++) {
      if (statesRxBinary[i].compareTo("1") == 0 &&
          _statesNames[i].compareTo("FREE") != 0 &&
          _statesNames[i].compareTo("") != 0 &&
          ((_statesNames[i].length < 12 ||
              _statesNames[i].substring(0, 12).compareTo("PROGRAMMABLE") !=
                  0))) {
        temp.add(_statesNames[i]);
      }
    }
    _currentActiveStates = temp;
  }

  Future<void> _updateCurrentActiveAlarms(Uint8List alarmsRx) async {
    _dataChanged = true;
    String alarmsRxBinary = "";
    for (var i = 3; i < alarmsRx.length - 2; i++) {
      alarmsRxBinary += uIntTo8bitString(alarmsRx[i]);
    }
    List<String> temp = [];
    for (int i = 0; i < alarmsRxBinary.length; i++) {
      if (alarmsRxBinary[i].compareTo("1") == 0 &&
          _alarmsNames[i].compareTo("FREE") != 0 &&
          _alarmsNames[i].compareTo("") != 0 &&
          ((_alarmsNames[i].length < 12 ||
              _alarmsNames[i].substring(0, 12).compareTo("PROGRAMMABLE") !=
                  0))) {
        temp.add(_alarmsNames[i]);
      }
    }
    _currentActiveAlarms = temp;
  }

/*
  Precondizioni: formatType può solo avere due valori: 0 o 1
*/
  Future<void> _updateCurrentActiveMeasures(Uint8List measuresRx) async {
    _dataChanged = true;
    List<int> values = [];
    for (int i = 3; i < measuresRx.length - 3; i += 2) {
      values.add(((measuresRx[i] & 0xff) << 8) | (measuresRx[i + 1] & 0xff));
    }
    _currentMeasureValues = Int16List.fromList(values);
    List<String> temp = [];
    for (int i = 0; i < _currentMeasureValues.length; i++) {
      if (_measurementsControlManagementTable[i].compareTo("1") == 0 &&
          _measureNames[i].compareTo("FREE") != 0 &&
          _measureNames[i].compareTo("") != 0 &&
          _measureNames[i].compareTo("Reserved") != 0) {
        if (i == 77) {
          temp.add(_handleM077Management());
        } else if (i == 79) {
          temp.add(_handleM079Management());
        } else {
          temp.add(_handleGeneralMeasurementManagement(i));
        }
      }
    }
    _currentMeasuresAllInOne = temp;
  }

  String _handleM077Management() {
    if ((_formatType == 0 &&
            _firstFormatOfMeasures[77].compareTo("LSB:MSB") == 0) ||
        (_formatType == 1 &&
            _firstFormatOfMeasures[77].compareTo("LSB:MSB") == 0)) {
      Uint8List bytes = uInt16ToTwoUInt8(intToUInt(_currentMeasureValues[77]));
      String msb = uIntTo8bitString(bytes[1]);
      String toInsert = _measureNames[77] + " " + bytes[0].toString();
      bool addedOne = false;
      if (msb[1].compareTo("1") == 0) {
        toInsert += ":" + binaryStringToUInt8(msb.substring(5)).toString();
        addedOne = true;
      }
      if (msb[0].compareTo("1") == 0) {
        if (addedOne) {
          toInsert += " " + binaryStringToUInt8(msb.substring(2, 5)).toString();
        } else {
          toInsert += ":" + binaryStringToUInt8(msb.substring(2, 5)).toString();
        }
      }
      return toInsert;
    }
    return "Unknown format type";
  }

  String _handleM079Management() {
    if (_currentMeasureValues[79] == 0) {
      return _measureNames[79] + " " + _currentMeasureValues[79].toString();
    } else {
      return _measureNames[79] +
          " A" +
          _currentMeasureValues[79]
              .toRadixString(16)
              .padLeft(4, "0")
              .toUpperCase();
    }
  }

  String _handleGeneralMeasurementManagement(int index) {
    String toInsert = _measureNames[index] + " ";
    toInsert += _formatType == 0
        ? _format(_currentMeasureValues[index], _firstFormatOfMeasures[index])
            .toString()
        : _format(_currentMeasureValues[index], _secondFormatOfMeasures[index])
            .toString();
    if (_unitOfMeasure[index].compareTo("") != 0) {
      toInsert += " " + _unitOfMeasure[index];
    }
    return toInsert;
  }

/*
  Precondizioni: il numero di occorrenze del '.' è al max 1, il numero di occorrenze
  dello ' ' è al max 1, se c'è un '.', questo sarà sempre dopo un EVENTUALE spazio,
  se c'è uno ' ', questo sarà sempre prima di un EVENTUALE punto; il numero di cifre
  di toFormat non può essere > del numero di cifre di pattern
*/
  String _format(int toFormat, String pattern) {
    pattern.replaceAll(" ", "");
    int formatLength = pattern.length - (pattern.contains(".") ? 1 : 0);
    String temp = toFormat.toString();
    bool hasSign = temp.contains("-");
    if (hasSign) {
      temp = temp.substring(1);
    }
    temp = temp.padLeft(formatLength, '0');
    int dotPos = pattern.indexOf(".");
    if (dotPos != -1) {
      temp = temp.substring(0, dotPos) + "." + temp.substring(dotPos);
    }
    if (hasSign) {
      temp = "-" + temp;
    }
    if (dotPos != -1) {
      return double.parse(temp)
          .toStringAsFixed(pattern.substring(dotPos + 1).length);
    } else {
      return double.parse(temp).toStringAsFixed(0);
    }
  }

  Future<void> _readMCMT(ModbusMaster? master) async {
    _dataChanged = true;
    Uint8List mcmtRx = await master!.readHoldingRegisters(0x00C0, 5);
    String mcmtRxBinary = "";
    for (var i = 3; i < mcmtRx.length - 2; i++) {
      mcmtRxBinary += uIntTo8bitString(mcmtRx[i]);
    }
    _measurementsControlManagementTable = mcmtRxBinary;
  }

  Future<void> _readFormatType(ModbusMaster? master) async {
    _dataChanged = true;
    Uint8List formatRx = await master!.readHoldingRegisters(0x000E, 1);
    _formatType = ((formatRx[3] & 0xff) << 8) | (formatRx[4] & 0xff);
  }

  void _reset() {
    if (!_resetRecently || _dataChanged) {
      _master = null;
      _resetRecently = true;
      _dataChanged = false;
      _currentActiveStates = [];
      _currentActiveAlarms = [];
      _currentMeasureValues = Int16List.fromList([]);
      _currentMeasuresAllInOne = [];
      _measurementsControlManagementTable = "";
      _formatType = -1;
    }
  }

  List<String> getCurrentActiveStates() => _currentActiveStates;

  List<String> getCurrentActiveAlarms() => _currentActiveAlarms;

  List<String> getCurrentMeasuresAllInOne() => _currentMeasuresAllInOne;

  ModbusMaster? get master => _master;

  set master(ModbusMaster? master) {
    _reset();
    if (master != null) {
      _master = master;
      _begin();
    }
  }
}