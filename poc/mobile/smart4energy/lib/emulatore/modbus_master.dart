import 'dart:async';
import 'dart:io';
import 'dart:typed_data';
import 'converter.dart';

class ModbusMaster {
  late final Socket _master;
  late final String _ipAddress;
  late final int _port;
  late final int _slaveId;
  late Completer _completer;
  bool _closed = false;

  ModbusMaster({required String ipAddress, required int port, required int slaveId}) {
    _completer = Completer<Uint8List>();
    _ipAddress = ipAddress;
    _port = port;
    _slaveId = slaveId;
  }

  String get getIpAddress {
    return _ipAddress;
  }

  int get getPort {
    return _port;
  }

  int get getSlaveId {
    return _slaveId;
  }

  bool get isClosed {
    return _closed;
  }

  Future<void> connect() async {
    _master = await Socket.connect(_ipAddress, _port);
    _master.listen((Uint8List frameRx) {
      _completer.complete(frameRx);
    }, onError: (error) {
      // ignore: avoid_print
      print(error);
      close();
    }, onDone: () {
      if (!_closed) {
        // ignore: avoid_print
        print("Slave Socket has been closed!");
        close();
      }
    }, cancelOnError: false);
  }

  Future<void> close() async {
    if (!_closed) {
      _closed = true;
      await _master.flush();
      await _master.close();
      _master.destroy();
    }
  }

  Uint8List _crc(Uint8List bytes) {
    var crc = BigInt.from(0xffff);
    var poly = BigInt.from(0xa001);
    for (var byte in bytes) {
      var bigByte = BigInt.from(byte);
      crc = crc ^ bigByte;
      for (int n = 0; n <= 7; n++) {
        int carry = crc.toInt() & 0x1;
        crc = crc >> 1;
        if (carry == 0x1) {
          crc = crc ^ poly;
        }
      }
    }
    var ret = Uint8List(2);
    ByteData.view(ret.buffer).setUint16(0, crc.toUnsigned(16).toInt());
    return ret;
  }

  Uint8List _createFrame(Uint8List frame) {
    Uint8List crc = _crc(frame);
    return Uint8List.fromList([
      frame,
      [crc[1]], //LSB
      [crc[0]] //MSB
    ].expand((x) => x).toList());
  }

  Future<Uint8List> readHoldingRegisters(int address, int wordNumber) async {
    _completer = Completer<Uint8List>();
    Uint8List frameTx = _createFrame(Uint8List.fromList([
      _slaveId,
      0x03,
      extractByteFromUInt16(address, 1),
      extractByteFromUInt16(address, 0),
      extractByteFromUInt16(wordNumber, 1),
      extractByteFromUInt16(wordNumber, 0)
    ]));
    _master.add(frameTx);
    return _completer.future.then((value) => value as Uint8List);
  }
}
