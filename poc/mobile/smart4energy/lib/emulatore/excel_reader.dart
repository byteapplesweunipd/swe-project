import 'dart:typed_data';
import 'package:excel/excel.dart';
import 'package:flutter/services.dart' show rootBundle;

Future<Excel> getExcel(String fileName) async {
  ByteData data = await rootBundle.load(fileName);
  return Excel.decodeBytes(data.buffer.asUint8List(data.offsetInBytes, data.lengthInBytes));
}

Sheet? getSheet(Excel excel, String sheetName) {
  return excel.tables[sheetName];
}

List<String> getColumnRange(
    Sheet? sheet, int firstRow, int lastRow, String columnName) {
  List<String> columnRange =
      List<String>.filled(lastRow - firstRow + 1, "", growable: false);
  dynamic temp;
  for (int row = firstRow, i = 0;
      row <= sheet!.maxRows && row <= lastRow;
      row++, i++) {
    temp = sheet.cell(CellIndex.indexByString("$columnName$row")).value;
    if (temp == null) {
      columnRange[i] = "";
    } else {
      columnRange[i] = temp;
    }
  }
  return columnRange;
}
