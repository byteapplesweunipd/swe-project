import 'package:flutter/material.dart';
import 'package:smart4energy/controllers/login_controller.dart';
import 'package:smart4energy/services/socket_service.dart';
import 'package:smart4energy/routes/app_pages.dart';
import 'package:get/get.dart';

Future<void> main() async {
  Get.put(LoginController());
  Get.put(SocketService());
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return GetMaterialApp(
      title: 'Smart4Energy',
      theme: ThemeData(
        primarySwatch: Colors.orange,
      ),
      getPages: AppPages.pages,
      initialRoute: AppPages.INITIAL_ROUTE,
    );
  }
}
