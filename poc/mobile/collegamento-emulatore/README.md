﻿# Collegamento mobile-emulatore v1

Nel file pubspec.yaml della cartella del progetto Flutter serve aggiungere la dipendenza:
- excel: ^2.0.0-null-safety-3

Nella cartella bin dell'emulatore bisogna aggiungere il file SLAVE01.xml che contiene i registri letti dallo slave su richiesta del master

Nella cartella del progetto Flutter va messo il file excel delle lingue (texts.xlsm) e quello delle misure (misure.xlsx)

Il file 'main.dart' è un esempio di utilizzo delle classi definite per il collegamento con l'emulatore
