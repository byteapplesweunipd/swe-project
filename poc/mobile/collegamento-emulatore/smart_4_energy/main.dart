import 'dart:async';
import 'lib/modbus_master.dart';
import 'lib/modbus_data_interpreter.dart';

// ignore_for_file: avoid_print

Future<void> main() async {
  //Creo il modbus master
  ModbusMaster master = ModbusMaster(ipAddress: "localhost", port: 8888, slaveId: 1);

  //Connetto il master allo slave
  await master.connect();

  //Creo l'interprete dei dati letti dal modbus master
  ModbusDataInterpreter interpreter = await ModbusDataInterpreter.create();

  //Imposto il modbus master nell'interprete
  interpreter.master = master;

  /*Da qui posso iniziare a leggere le liste degli stati, allarmi e misure,
  * che continueranno ad aggiornarsi fino a quando il master non sarà chiuso
  * o fino a quando nell'interprete verrà settato un nuovo modbus master*/

  //Aspetto qualche secondo in modo da dare il tempo all'interprete di iniziare a
  //leggere i dati, altrimenti leggerei delle liste vuote
  await Future.delayed(const Duration(seconds: 3));

  print("STATI");
  print(interpreter.getCurrentActiveStates());
  print("-----------------------");
  print("ALLARMI");
  print(interpreter.getCurrentActiveAlarms());
  print("-----------------------");
  print("MISURE");
  print(interpreter.getCurrentMeasuresAllInOne());
  print("-----------------------");

  //Aspetto altri 3 secondi e poi chiudo il collegamento con lo slave
  await Future.delayed(const Duration(seconds: 3));

  await master.close();
}
